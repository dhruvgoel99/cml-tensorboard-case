# CML with Tensorboard use case

This repository contains a sample project using [CML](https://github.com/iterative/cml) with Tensorboard.dev to track model training in real-time. When a pull request is made, the following steps occur:
- GitLab will deploy a runner machine with a specified CML Docker environment
- A Tensorboard.dev page will be created 
- CML will report a link to the Tensorboard as a comment in the pull request
- The runner will execute a workflow to train a ML model (`python train.py`)

The key file enabling these actions is `.gitlab-ci.yml`.

## CI/CD variables
In this example, the workflow requires two CI/CD variables.

| Variable  | Description  | 
|---|---|
|  repo_token | A personal access token with api, read_repository, and write_repository privileges.  |
| TB_CREDENTIALS  | Tensorboard credentials | 

⚠️ Variables should be masked but not protected. 

To access your Tensorboard credentials:
1. On your local machine, run `tensorboard dev upload` 
2. Accept the TOS and follow the authentication procedure. 
3. When you have authenticated, copy your credentials out of `~/.config/tensorboard/credentials/uploader-creds.json` (this is the typical path for OSX/Linux systems). Paste these credentials as the variable TB_CREDENTIALS. 


